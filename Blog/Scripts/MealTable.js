﻿$(document).ready(function () {

    var calcMeal = new Array();
    var price = 0;
    var kalorie = 0;
    var protein = 0;
    var carbo = 0;
    var fat = 0;
    var allMakro = 0;
    pieChart();

    function showMeals(data) {
        var meals;
        $.each(data, function (k, v) {
            meals += "<tr data-meal-id=" + v.mealId + "><td>" + v.mealName + "</td> <td>" + v.category.categoryName
                + "</td> <td>" + v.price + "</td> <td>" + v.kalorie + "</td> <td>" + v.protein
                + "</td> <td>" + v.carbo + "</td> <td>" + v.fat + "</td></tr>";
        });
        $("#js-meal tbody").html(meals);
    }

    function showResult() {
        $("#js-result p span:eq(0)").text(kalorie);
        $("#js-result p span:eq(1)").text(price);
        $("#js-result p span:eq(2)").text(protein.toFixed(2));
        $("#js-result p span:eq(3)").text(carbo.toFixed(2));
        $("#js-result p span:eq(4)").text(fat.toFixed(2));
        $("#js-result p span:eq(5)").text(Math.round(kalorie / price));
        $("#js-result p span:eq(6)").text((protein / price).toFixed(2));
        $("#js-result p span:eq(7)").text((carbo / price).toFixed(2));
        $("#js-result p span:eq(8)").text((fat / price).toFixed(2));
    }

    $.get("/api/meal/search")
        .done(function (data) {
            showMeals(data);
        })
        .fail(function () {
            alert("Coś nie poszło.");
        });

    $("#js-meal tbody").on("click", "tr", function () {
        $.get("/api/meal/meal/" + $(this).attr("data-meal-id"))
            .done(function (data) {
                calcMeal[calcMeal.length] = data;
                var list = "";
                price = 0;
                kalorie = 0;
                protein = 0;
                carbo = 0;
                fat = 0;
                for (var i in calcMeal) {
                    allMakro = 0;
                    if (calcMeal[i].mealName == "")
                        continue;
                    list += "<li data-index-meal=" + i + ">" + calcMeal[i].mealName
                        + "&nbsp<span class='js-list-remove glyphicon glyphicon-remove' style='color:red'></span></li>";
                    kalorie += calcMeal[i].kalorie;
                    price += calcMeal[i].price;
                    protein += calcMeal[i].protein;
                    carbo += calcMeal[i].carbo;
                    fat += calcMeal[i].fat;
                    pieChart();
                }
                $("#js-choices").html(list);
                showResult();
            })
            .fail(function () {
                alert("cos nie tak");
            });
    });

    $("#js-choices").on("click", ".js-list-remove", function () {
        var index = $(this).parents("li").attr("data-index-meal");
        $(this).parents("li").remove();
        kalorie -= calcMeal[index].kalorie;
        price -= calcMeal[index].price;
        protein -= calcMeal[index].protein;
        carbo -= calcMeal[index].carbo;
        fat -= calcMeal[index].fat;
        calcMeal[index].mealName = "";
        pieChart();
        showResult();
    });

    $("#js-search").on("keyup", function () {
        var word = $("#js-search").val();
        if (word == "") {
            $.get("/api/meal")
                .done(function (data) {
                    showMeals(data);
                })
                .error(function () {
                    alert("Coś nie poszło.");
                });
        } else {
            $.get("/api/meal/search/" + word)
                .done(function (data) {
                    showMeals(data);
                })
                .fail(function () {
                    $("#js-meal tbody").html("<tr><td colspan=7>Brak wyników</td</tr>");
                });
        }
    });

    function pieChart() {
        allMakro = protein + carbo + fat;

        // Make monochrome colors
        var pieColors = (function () {
            var colors = [],
                base = Highcharts.getOptions().colors[8],
                i;

            for (i = 0; i < 10; i += 1) {
                // Start out with a darkened base color (negative brighten), and end
                // up with a much brighter color
                colors.push(Highcharts.Color(base).brighten((i - 2) / 6).get());
            }
            return colors;
        }());

        // Build the chart
        Highcharts.chart('pie-chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Rozkład makroskładników'
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: false,
                    cursor: 'pointer',
                    colors: pieColors,
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                        distance: -50,
                        filter: {
                            property: 'percentage',
                            operator: '>',
                            value: 4
                        }
                    }
                }
            },
            series: [{
                name: 'Brands',
                data: [
                    { name: 'Białko', y: protein / allMakro * 100 },
                    { name: 'Węgle', y: carbo / allMakro * 100 },
                    { name: 'Tłuszcz', y: fat / allMakro * 100 }
                ]
            }]
        });
    }
});