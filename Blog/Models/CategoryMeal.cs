﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class CategoryMeal
    {
        [Key]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        //public virtual ICollection<Meal> MyProperty { get; set; }
    }
}