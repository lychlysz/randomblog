﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Meal
    {
        public int MealId { get; set; }
        public string MealName { get; set; }
        public float Price { get; set; }
        public int Kalorie { get; set; }
        public float Protein { get; set; }
        public float Carbo { get; set; }
        public float Fat { get; set; }
        public int CategoryId { get; set; }

        public CategoryMeal Category { get; set; }

    }
}