﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Models
{
    public class Post
    {
        public Post()
        {
            this.Comments = new HashSet<Comment>();
        }

        public int PostId { get; set; }

        [Required(ErrorMessage = "Musisz wpisać coś w Tytule"), Display(Name = "Tytuł Postu")]
        [StringLength(70, ErrorMessage = "Maksymalna liczba znaków: 70")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Musisz wpisać coś w poście"), Display(Name = "Post")]
        [StringLength(5000, ErrorMessage = "Maksymalna liczba znaków: 5000")]
        [AllowHtml]
        public string Text { get; set; }

        [Required, Display(Name = "Data")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Kim jesteś?"), Display(Name = "Autor")]
        public string Author { get; set; }
        public int Likes { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}