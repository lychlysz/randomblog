﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Blog.Models.Service
{
    public class Service
    {
        private ApplicationDbContext _context = new ApplicationDbContext();
        
        public List<Post> GetPosts()
        {
            var posts = _context.Posts.OrderByDescending(p=>p.Date).ToList();

            return posts;
        }

        public List<Post> GetPosts(int count)
        {
            var posts = _context.Posts.Take(count).OrderByDescending(p => p.Date).ToList();
            

            return posts;
        }

        public Post GetPostById(int id)
        {
            var post = _context.Posts.SingleOrDefault(p => p.PostId == id);

            return post;
        }

        public List<Comment> GetCommentsByPostId(int postId)
        {
            var comments = _context.Comments
                .Where(p => p.PostId == postId)
                .OrderByDescending(p => p.Date)
                .ToList();

            return comments;
        }

        public void AddComment(Comment comment)
        {
            _context.Comments.Add(comment);
            _context.SaveChanges();
        }

        public void AddPost(Post post)
        {
            _context.Posts.Add(post);
            _context.SaveChanges();
        }       

    }
}