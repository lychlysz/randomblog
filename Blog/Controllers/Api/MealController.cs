﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using Blog.Models;
using System.Data.Entity;

namespace Blog.Controllers.Api
{
    public class MealController : ApiController
    {
        private ApplicationDbContext _context;

        public MealController()
        {
            _context = new ApplicationDbContext();
        }         
        
        [HttpGet]
        public IHttpActionResult Meal(int word)
        {
            var meal = _context.Meals.SingleOrDefault(b => b.MealId == word);
            if (meal == null)
                return NotFound();

            return Ok(meal);
        }

        [HttpPost]
        public IHttpActionResult AddNew(Meal meal)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _context.Meals.Add(meal);
            _context.SaveChanges();

            var mealInDb = _context.Meals.Include(m => m.Category).ToList().LastOrDefault();
            if (mealInDb == null)
                return NotFound();

            return Ok(mealInDb);
        }

        [HttpGet]
        public IHttpActionResult Search(string word = null)
        {
            var meals = _context.Meals.Include(m=> m.Category).ToList();
            if (!String.IsNullOrWhiteSpace(word))
                meals = meals.Where(b => b.MealName.ToLowerInvariant().Contains(word)).ToList();
            if (meals.Count == 0)
                return NotFound();        
            
            return Ok(meals);
        }

        [Authorize(Roles ="Admin")]
        [HttpDelete]
        public IHttpActionResult RemoveBurger(int id)
        {
            var mealInDb = _context.Meals.SingleOrDefault(b => b.MealId == id);
            if (mealInDb == null)
                return NotFound();

            _context.Meals.Remove(mealInDb);
            _context.SaveChanges();
            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IHttpActionResult GetCategory()
        {
            var categories = _context.Categories.ToList();
            return Ok(categories);
        }
    }
}
