﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Blog.Models;

namespace Blog.Controllers.Api
{
    public class LikeController : ApiController
    {
        private ApplicationDbContext _context;
        public LikeController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult showLike(PostLike postLike)
        {
            if (postLike.PostId == 0)
            {
                var hands = _context.PostLikes.Where(h=>h.UserLogin.Equals(postLike.UserLogin)).ToList();
                return Ok(hands);
            }
            else
            {
                var hand = _context.PostLikes.SingleOrDefault(h => h.UserLogin.Equals(postLike.UserLogin) && h.PostId == postLike.PostId);
                return Ok(hand);
            }
        }

        [HttpPost]
        public IHttpActionResult addLike(PostLike postLike)
        {
            var like = _context.PostLikes.SingleOrDefault(l => l.PostId == postLike.PostId && l.UserLogin.Equals(postLike.UserLogin));
            var post = _context.Posts.SingleOrDefault(p => p.PostId == postLike.PostId);
            if (post == null)
                return NotFound();

            if (like == null)
            {
                _context.PostLikes.Add(postLike);
                if (postLike.IsLike)
                    post.Likes++;
                else
                    post.Likes--;
            }
            else
            {
                if (like.IsLike == true && postLike.IsLike == false)
                {
                    like.IsLike = false;
                    post.Likes--;
                }
                else if (like.IsLike == false && postLike.IsLike == true)
                {
                    like.IsLike = true;
                    post.Likes++;
                }
                else
                {
                    return BadRequest("Już tak głosowano");
                }
            }
            _context.SaveChanges();
            return Ok(post);
        }

    }
}
