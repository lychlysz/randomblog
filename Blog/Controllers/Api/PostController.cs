﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using Blog.Models;
using System.Data.Entity;

namespace Blog.Controllers.Api
{
    public class PostController : ApiController
    {

        private ApplicationDbContext _context;

        public PostController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpGet]
        public IHttpActionResult Posts()
        {
            var post = _context.Posts.ToList().OrderByDescending(p => p.Date).FirstOrDefault();          
            if (post == null)
                NotFound();
            Thread.Sleep(2000);

            return Ok(post);
        }

        [HttpGet]
        public IHttpActionResult Posts(int id)
        {
            var post = _context.Posts.Include(p=> p.Comments).SingleOrDefault(p => p.PostId == id);
            if (post == null)
                return NotFound();

            return Ok(post);
        }
    }
}
