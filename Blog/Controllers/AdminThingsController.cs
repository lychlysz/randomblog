﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Blog.Models;
using Blog.Models.Service;

namespace Blog.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminThingsController : Controller
    {
        private Service service = new Service();

        public ActionResult PostCreator()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Posted(Post post)
        {
            post.Date = DateTime.Now;
            post.Likes = 0;

            if (!ModelState.IsValid)
                return View("PostCreator", post);

            service.AddPost(post);

            return RedirectToAction("Posts", "MainContent");
        }
    }
}