﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Models;
using Blog.Models.Service;
using Blog.ViewModels;

namespace Blog.Controllers
{
    public class MainContentController : Controller
    {
        private Service service = new Service();

        public ActionResult Index()
        {
            var posts = service.GetPosts(5);

            return View(posts);
        }

        public ActionResult Posts()
        {
            var posts = service.GetPosts();
            return View(posts);
        }

        public ActionResult ShowPost(int id)
        {
            var post = service.GetPostById(id);
            if (post == null)
                return HttpNotFound();
                       
            var vm = new PostCommentViewModel
            {
                Post = post,
                Comment = new Comment(),
                Comments = service.GetCommentsByPostId(post.PostId),
            };

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveComment(Comment comment, int id)
        {
            comment.Likes = 0;
            comment.Date = DateTime.Now;
            comment.PostId = id;
            comment.User = User.Identity.Name;

            //if (!ModelState.IsValid)
            //    return RedirectToAction("ShowPost", new { id = id });

            service.AddComment(comment);

            return RedirectToAction("ShowPost", new { id = id });
        }
    }
}