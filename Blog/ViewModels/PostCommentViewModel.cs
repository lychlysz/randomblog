﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models;

namespace Blog.ViewModels
{
    public class PostCommentViewModel
    {
        public Post Post { get; set; }
        public Comment Comment { get; set; }
        public List<Comment> Comments { get; set; }
        public PostLike PostLike { get; set; }
    }
}