namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class burgerChangingIntoMeal : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Meals",
                c => new
                    {
                        MealId = c.Int(nullable: false, identity: true),
                        MealName = c.String(),
                        Price = c.Single(nullable: false),
                        Kalorie = c.Int(nullable: false),
                        Protein = c.Int(nullable: false),
                        Carbo = c.Int(nullable: false),
                        Fat = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MealId)
                .ForeignKey("dbo.CategoryMeals", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.CategoryMeals",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            DropTable("dbo.Burgers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Burgers",
                c => new
                    {
                        BurgerId = c.Int(nullable: false, identity: true),
                        BurgerName = c.String(),
                        Price = c.Single(nullable: false),
                        Kalorie = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BurgerId);
            
            DropForeignKey("dbo.Meals", "CategoryId", "dbo.CategoryMeals");
            DropIndex("dbo.Meals", new[] { "CategoryId" });
            DropTable("dbo.CategoryMeals");
            DropTable("dbo.Meals");
        }
    }
}
