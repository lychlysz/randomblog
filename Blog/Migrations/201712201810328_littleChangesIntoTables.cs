namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class littleChangesIntoTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 70));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "Title");
        }
    }
}
