namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeCommentUserRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "User", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "User", c => c.String(nullable: false));
        }
    }
}
