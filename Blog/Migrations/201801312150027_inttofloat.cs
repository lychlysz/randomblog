namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inttofloat : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Meals", "Protein", c => c.Single(nullable: false));
            AlterColumn("dbo.Meals", "Carbo", c => c.Single(nullable: false));
            AlterColumn("dbo.Meals", "Fat", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Meals", "Fat", c => c.Int(nullable: false));
            AlterColumn("dbo.Meals", "Carbo", c => c.Int(nullable: false));
            AlterColumn("dbo.Meals", "Protein", c => c.Int(nullable: false));
        }
    }
}
