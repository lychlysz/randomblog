namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBurgerTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Burgers",
                c => new
                    {
                        BurgerId = c.Int(nullable: false, identity: true),
                        BurgerName = c.String(),
                        Price = c.Single(nullable: false),
                        Kalorie = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BurgerId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Burgers");
        }
    }
}
